#!/bin/bash
dir=$(pwd);

if [[ $dir == *"vendor/bin"* ]]; then
        executable="../intaglio/intaglio-core/bin/bolt";
else
        executable="vendor/intaglio/intaglio-core/bin/bolt";
fi

touch .hhconfig;

$executable "$@";