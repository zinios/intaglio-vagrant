#!/bin/bash

host_os=$1;

echo "Installing and configuring Intaglio SSH key"
cd ~
rm .hhvm.hhbc

cd .ssh
cp /vagrant/provision/key id_rsa
cp /vagrant/provision/known_hosts known_hosts

chmod 600 id_rsa
chmod 600 known_hosts

echo "Creating /srv structure"
sudo mkdir -p /srv/www/intaglio.dev.lan
sudo chown -R vagrant:www-data /srv/www

echo "Configuring nginx"
sudo cp /vagrant/provision/intaglio.dev.lan.conf /etc/nginx/sites-available/intaglio.dev.lan.conf
cd /etc/nginx/sites-enabled
sudo ln -s ../sites-available/intaglio.dev.lan.conf

cd /srv/www/intaglio.dev.lan

sudo cp /vagrant/provision/composer.json composer.json

mkdir ./public
cp -R /vagrant/provision/public/. ./public
touch .hhconfig

alias composer='hhvm /usr/local/bin/composer'
hhvm /usr/local/bin/composer install

# if [[ host_os=="windows" ]]; then
# 	rm /srv/www/intaglio.dev.lan/vendor/bin/nuclio
# 	sudo cp /vagrant/provision/composer_bin /srv/www/intaglio.dev.lan/vendor/bin/nuclio
# fi

if [[ host_os="windows" ]]; then
	rm /srv/www/intaglio.dev.lan/vendor/bin/nuclio
	rm /srv/www/intaglio.dev.lan/vendor/bin/bolt
	sudo cp /vagrant/provision/nuclio_bin.sh /srv/www/intaglio.dev.lan/vendor/bin/nuclio
	sudo cp /vagrant/provision/bolt_bin.sh /srv/www/intaglio.dev.lan/vendor/bin/bolt
	sed -i -e 's/\r$//' /srv/www/intaglio.dev.lan/vendor/bin/nuclio
	sed -i -e 's/\r$//' /srv/www/intaglio.dev.lan/vendor/bin/bolt
fi


hhvm vendor/bin/nuclio setup --config ./hello/config
hhvm vendor/bin/bolt build
hhvm vendor/bin/bolt reinstall --application \
									intaglio\\Intaglio \
									workbench\\Workbench \
								--plugin \
									workbench\\plugin\\fullscreen\\Fullscreen \
									workbench\\plugin\\dashboard\\Dashboard \
									workbench\\plugin\\user\\User \
									workbench\\plugin\\userActivity\\UserActivity \
									workbench\\plugin\\permission\\Permission \
									workbench\\plugin\\data\\Data \
									workbench\\plugin\\services\\Services \
									workbench\\plugin\\navigation\\Navigation \
									workbench\\plugin\\workflow\\Workflow \
									workbench\\plugin\\fileSystem\\FileSystem


sudo hhvm vendor/bin/bolt addSite \
						--name Intaglio Development \
						--domain intaglio.dev.lan \
						--service 	ADMIN \
									WORKBENCH \
									WORKBENCH_PLUGIN \
									SETTINGS \
									API \
									ROOT \
						--rootService ADMIN

echo "Intaglio setup complete!"
